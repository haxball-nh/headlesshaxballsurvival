import { Goal, Stadium } from "../models/Stadium";
export class StadiumBuilder {
  public static readonly BIG_BALL_SIZE = 10;
  public static readonly FUTSAL_BALL_SIZE = 6.4;

  public static buildStadium(
    stadiumName: string,
    ballRadius: number,
    pitchData: string,
    goalAreas: Goal[]
  ): Stadium {
    return {
      name: stadiumName,
      ballRadius: ballRadius,
      currentStadium: false,
      goalAreas: goalAreas,
      pitchData: pitchData,
    } as Stadium;
  }

  public static buildGoalArea(
    goalNumber: number,
    playerSpawn: number[],
    upperLeftCoord: number[],
    upperRightCoord: number[],
    lowerLeftCoord: number[],
    lowerRightCoord: number[]
  ): Goal {
    return {
      goalNo: goalNumber,
      playerSpawn: playerSpawn,
      goalArea: {
        upperLeftCoord: {
          x: upperLeftCoord[0],
          y: upperLeftCoord[1],
        },
        upperRightCoord: {
          x: upperRightCoord[0],
          y: upperRightCoord[1],
        },
        lowerLeftCoord: {
          x: lowerLeftCoord[0],
          y: lowerLeftCoord[1],
        },
        lowerRightCoord: {
          x: lowerRightCoord[0],
          y: lowerRightCoord[1],
        },
      },
    };
  }
}
