import { Player } from "../models/haxball/Player";
import { chatCommandService } from "../services/ChatCommandService";
import { room } from "../index";
import { announcementService } from "../services/AnnouncementService";
import { SurvivorListStore } from "../store/SurvivorListStore";

export const registerPlayerEventHandlers = () => {
  registerOnPlayerJoin();
  registerOnPlayerLeave();
  registerOnPlayerChat();
};

const registerOnPlayerJoin = () => {
  room.onPlayerJoin = (player: Player) => {
    try {
      announcementService.sendWelcomeAnnouncement(player.id);
    } catch (error) {
      console.log(error);
    }
  };
};
const registerOnPlayerLeave = () => {
  room.onPlayerLeave = (player: Player) => {
    try {
      SurvivorListStore.removePlayer(player.id);
    } catch (error) {
      console.log(error);
    }
  };
};
const registerOnPlayerChat = () => {
  room.onPlayerChat = (player: Player, message: string) => {
    try {
      return chatCommandService.playerChat(player, message);
    } catch (error) {
      console.log(error);
    }
  };
};
