import { Player } from "../models/haxball/Player";
import { Team } from "../models/haxball/Team";
import { room } from "../index";
import { goalService } from "../services/GoalService";
import { roomManagementService } from "../services/RoomManagementService";
import { gameManagementService } from "../services/GameManagementService";

export const registerGameMechanicsEventHandlers = () => {
  registerOnTeamGoal();
  registerOnGameStart();
  registerOnPositionsReset();
};

const registerOnTeamGoal = () => {
  room.onTeamGoal = async (_: Team) => {
    try {
      await goalService.handleGoal();
    } catch (error) {
      console.log(error);
    }
  };
};
const registerOnPositionsReset = () => {
  room.onPositionsReset = () => {
    try {
      gameManagementService.resetRound();
    } catch (error) {
      console.log(error);
    }
  };
};

const registerOnGameStart = () => {
  room.onGameStart = (_: Player) => {
    try {
      roomManagementService.movePlayersIntoPosition();
    } catch (error) {
      console.log(error);
    }
  };
};
