export interface Stadium {
  name: StadiumName;
  ballRadius: number;
  goalAreas: Goal[];
  pitchData: string;
  currentStadium: boolean;
}

export interface Goal {
  goalNo: number;
  playerSpawn: number[];
  goalArea: GoalArea;
}

export interface GoalArea {
  upperLeftCoord: {
    x: number;
    y: number;
  };
  upperRightCoord: {
    x: number;
    y: number;
  };
  lowerLeftCoord: {
    x: number;
    y: number;
  };
  lowerRightCoord: {
    x: number;
    y: number;
  };
}

export enum StadiumName {
  SURVIVAL_2 = "SURVIVAL_2",
  SURVIVAL_3 = "SURVIVAL_3",
  SURVIVAL_4 = "SURVIVAL_4",
  SURVIVAL_5 = "SURVIVAL_5",
  SURVIVAL_6 = "SURVIVAL_6",
  SURVIVAL_7 = "SURVIVAL_7",
  SURVIVAL_8 = "SURVIVAL_8",
  SURVIVAL_9 = "SURVIVAL_9",
  SURVIVAL_10 = "SURVIVAL_10",
}
