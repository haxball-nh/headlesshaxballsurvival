import { roomManagementService } from "./RoomManagementService";
import { Player } from "../models/haxball/Player";
import { SurvivorListStore } from "../store/SurvivorListStore";
import { gameManagementService } from "./GameManagementService";

export class ChatCommandService {
  public playerChat(player: Player, message: string): boolean {
    if (message.trim() === "+") {
      this.registerPlayer(player);
      return false;
    } else if (message === "!reset") {
      roomManagementService.resetRoom();
    }
    return true;
  }
  private registerPlayer(player: Player): void {
    SurvivorListStore.addPlayer(player);
    gameManagementService.runGameMode();
  }
}

export const chatCommandService = new ChatCommandService();
