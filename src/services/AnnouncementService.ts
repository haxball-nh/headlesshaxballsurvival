import { room } from "../index";
import { gameManagementService, GameState } from "./GameManagementService";

export class AnnouncementService {
  public sendAnnouncement(message: string): void {
    room.sendAnnouncement(message);
  }

  public sendWinnerAnnouncement(playerName: string): void {
    room.sendAnnouncement(`╔═════════════════ 🏆 ════════════════  ᴀᴡᴀʀᴅs  ═════════════════ 🏆 ═════════════════
║\t\t\t\t\t\t\t\t\t\tCHAMPION    👑    ${playerName} (last survivor)
╚═════════════════ 🏆 ════════════════════════════════════════ 🏆 ════════════════`);
  }

  public sendPlayerHurtAnnouncement(playerName: string): void {
    room.sendAnnouncement(`🩸 ${playerName} has been hurt`);
  }

  public sendPlayerEliminatedAnnouncement(playerName: string): void {
    room.sendAnnouncement(`⚰️ ${playerName} has been eliminated`);
  }

  public sendJoinGameAnnouncement(): void {
    room.sendAnnouncement(
      `░░░░░░░░░░░░░░░░░░░░░░░░▒                ᴛʏᴘᴇ 【 + 】 ᴛᴏ ᴊᴏɪɴ                ▒░░░░░░░░░░░░░░░░░░░░░░░`
    );
  }

  public sendWelcomeAnnouncement(playerId: number): void {
    room.sendAnnouncement(
      `░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░▒█░░▒█▒█▀▀▒█░░▒█▀▀▒█▀▀█▒██▄██▒█▀▀▒█░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░▒█░█▒█▒█▄▄▒█░░▒█░░░█░░█▒█░█▒█▒█▄▄▒█░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░▒▀▄▀▄▀▒█▄▄▒█▄▄▒█▄▄▒█▄▄█▒█░░▒█▒█▄▄▒▄░░░░░░░░░░░░░░░░░░░░░░░`,
      playerId
    );
    if (gameManagementService.currentGameState === GameState.IN_GAME)
      room.sendAnnouncement(
        `░░░░░░░░░░░░░░░░░░░░░░░▒     a ɢᴀᴍᴇ ɪs ᴄᴜʀʀᴇɴᴛʟʏ ɪɴ ᴘʀᴏɢʀᴇss - ᴘʟᴇᴀsᴇ ᴡᴀɪᴛ     ▒░░░░░░░░░░░░░░░░░░░░░░`,
        playerId
      );
    else
      room.sendAnnouncement(
        `░░░░░░░░░░░░░░░░░░░░░░░░░░░▒                ᴛʏᴘᴇ 【 + 】 ᴛᴏ ᴊᴏɪɴ                ▒░░░░░░░░░░░░░░░░░░░░░░░░░░`,
        playerId
      );
  }
  public sendGameInitialised(): void {
    room.sendAnnouncement(
      `🎬 Game request initiated | Rounds will start in 15 seconds if at least 3 players have joined`
    );
  }
  public sendGameStartingAnnouncement(): void {
    room.sendAnnouncement(
      `⏱️ Minimum players joined! | Game will start soon...`
    );
  }
  public sendCountDownAnnouncement(timeRemaining: number): void {
    const noBlank = timeRemaining;
    const noFilledIn = 15 - noBlank;
    let message = `🎬️`;
    for (let i = 0; i < noFilledIn; i++) {
      message += `▰`;
    }
    for (let i = 0; i < noBlank; i++) {
      message += `▱`;
    }
    message += ` | ${timeRemaining} seconds remaining until round begins`;

    room.sendAnnouncement(message);
  }
  public sendStartingAnnouncement(): void {
    room.sendAnnouncement(`🎬️ ▰▰▰▰▰▰▰▰▰▰▰▰ | Starting!`);
  }
}

export const announcementService = new AnnouncementService();
