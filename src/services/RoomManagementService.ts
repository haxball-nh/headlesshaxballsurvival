import { StadiumStore } from "../store/StadiumStore";
import { Stadium, StadiumName } from "../models/Stadium";
import { Team } from "../models/haxball/Team";
import { room } from "../index";
import { SurvivorListStore } from "../store/SurvivorListStore";
import { gameManagementService } from "./GameManagementService";

export class RoomManagementService {
  public resetRoom(): void {
    const nantBlue = 0x1e4288;
    const nantOrange = 0xf7901e;
    const white = 0xffffff;

    room.setScoreLimit(14);
    room.setTimeLimit(14);
    room.setTeamColors(Team.RedTeam, 60, white, [
      nantOrange,
      nantBlue,
      nantOrange,
    ]);
    room.setTeamColors(Team.BlueTeam, 60, white, [
      nantOrange,
      nantBlue,
      nantOrange,
    ]);

    room.setTeamsLock(true);

    gameManagementService.resetGame();
  }

  public setStadium(stadiumName: StadiumName): void {
    const stadium = StadiumStore.getStadium(stadiumName);
    if (stadium) {
      room.setCustomStadium(stadium.pitchData);
      StadiumStore.setCurrentStadium(stadiumName);
    }
  }

  public setAvatar(playerId: number, avatar: string | null): void {
    room.setPlayerAvatar(playerId, avatar);
  }

  public movePlayersIntoPosition(): void {
    room.pauseGame(true);
    const players = SurvivorListStore.getPlayerList();
    const currentStadium = StadiumStore.getCurrentStadium() as Stadium;

    for (let i = 0; i < players.length; i++) {
      const player = players[i];
      const goalArea = currentStadium.goalAreas.filter(
        (goal) => goal.goalNo === i + 1
      )[0];

      room.setPlayerDiscProperties(player.playerId, {
        x: goalArea.playerSpawn[0],
        y: goalArea.playerSpawn[1],
      });
    }
    room.pauseGame(false);
  }
}

export const roomManagementService = new RoomManagementService();
