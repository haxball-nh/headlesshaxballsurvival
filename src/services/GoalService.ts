import { room } from "../index";
import { StadiumStore } from "../store/StadiumStore";
import { GoalArea, Stadium } from "../models/Stadium";
import { Disc } from "../models/haxball/Disc";
import { SurvivorListStore } from "../store/SurvivorListStore";

export class GoalService {
  public async handleGoal(): Promise<void> {
    const goalId = GoalService.getGoalId();
    await SurvivorListStore.removeLife(goalId);
  }

  private static getGoalId(): number {
    const ball = room.getDiscProperties(0);
    const stadium = StadiumStore.getCurrentStadium() as Stadium;

    for (const goal of stadium.goalAreas) {
      if (this.isBallInGoal(ball, goal.goalArea)) return goal.goalNo;
    }
    return 0;
  }

  private static isBallInGoal(ball: Disc, goalArea: GoalArea): boolean {
    const ballX = ball.x as number,
      ballY = ball.y as number;

    // is right and down of upper left
    const withinUpperLeft =
      this.isRightOf(ballX, goalArea.upperLeftCoord.x) &&
      this.isDownOf(ballY, goalArea.upperLeftCoord.y);

    // is left and down of upper right
    const withinUpperRight =
      this.isLeftOf(ballX, goalArea.upperRightCoord.x) &&
      this.isDownOf(ballY, goalArea.upperRightCoord.y);

    // is right and up of lower left
    const withinLowerLeft =
      this.isRightOf(ballX, goalArea.lowerLeftCoord.x) &&
      this.isUpOf(ballY, goalArea.lowerLeftCoord.y);

    // is left and up of lower right
    const withinLowerRight =
      this.isLeftOf(ballX, goalArea.lowerRightCoord.x) &&
      this.isUpOf(ballY, goalArea.lowerRightCoord.y);

    return (
      withinUpperLeft && withinUpperRight && withinLowerLeft && withinLowerRight
    );
  }

  private static isRightOf(ballX: number, goalX: number): boolean {
    return ballX > goalX;
  }

  private static isLeftOf(ballX: number, goalX: number): boolean {
    return ballX < goalX;
  }

  private static isDownOf(ballY: number, goalY: number): boolean {
    return ballY > goalY;
  }

  private static isUpOf(ballY: number, goalY: number): boolean {
    return ballY < goalY;
  }
}

export const goalService = new GoalService();
