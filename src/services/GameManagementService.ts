import { Survivor, SurvivorListStore } from "../store/SurvivorListStore";
import { Stadium, StadiumName } from "../models/Stadium";
import { room } from "../index";
import { roomManagementService } from "./RoomManagementService";
import { StadiumStore } from "../store/StadiumStore";
import { announcementService } from "./AnnouncementService";
import { TimeUtilities } from "../utilities/TimeUtilities";

export enum GameState {
  LOBBY,
  COUNTDOWN,
  IN_GAME,
}

export class GameManagementService {
  private readonly lobbyTimerSecs: number;
  private intervalId: NodeJS.Timeout | undefined;
  public currentGameState: GameState = GameState.LOBBY;

  constructor() {
    this.lobbyTimerSecs = 15;
  }

  public runGameMode(): void {
    if (this.currentGameState === GameState.LOBBY) {
      const players = SurvivorListStore.getPlayerList();
      if (players.length === 1) {
        announcementService.sendGameInitialised();
      }
      // Get list of players signed up for game
      if (players.length >= 3) {
        announcementService.sendGameStartingAnnouncement();
        // start countdown
        this.currentGameState = GameState.COUNTDOWN;
        this.setTimer(this.lobbyTimerSecs);
      }
    }
  }

  public resetRound(): void {
    const players = SurvivorListStore.getPlayerList();

    // change map if needed
    if (this.needChangeMap(players.length)) {
      room.stopGame();
      this.setCorrectStadium(players.length);
      room.startGame();
    }

    // move players into position
    roomManagementService.movePlayersIntoPosition();
    this.setLivesOnAvatars(players);
  }

  public async setGameWon(): Promise<void> {
    const winner = room.getPlayer(
      SurvivorListStore.getPlayerList()[0].playerId
    );
    roomManagementService.setAvatar(winner.id, `👑`);
    announcementService.sendWinnerAnnouncement(winner.name);

    await TimeUtilities.delay(2000);

    this.resetGame();
  }

  public resetGame(): void {
    room.stopGame();
    clearInterval(this.intervalId as NodeJS.Timeout);
    SurvivorListStore.resetPlayerList();
    this.setAllPlayersToSpectators();

    this.currentGameState = GameState.LOBBY;
    announcementService.sendJoinGameAnnouncement();
  }

  private setAllPlayersToSpectators(): void {
    room.getPlayerList().forEach((player) => {
      room.setPlayerTeam(player.id, 0);
    });
  }

  private setTimer(counter: number) {
    this.intervalId = setInterval(() => {
      counter = counter - 1;
      switch (counter) {
        case 10:
        case 5:
        case 3:
        case 2:
        case 1:
          announcementService.sendCountDownAnnouncement(counter);
          break;
        case 0:
          announcementService.sendStartingAnnouncement();
          clearInterval(this.intervalId as NodeJS.Timeout);
          this.startGame();
          break;
      }
    }, 1000);
  }

  private startGame(): void {
    this.currentGameState = GameState.IN_GAME;
    const players = SurvivorListStore.getPlayerList();

    this.setCorrectStadium(players.length);
    this.setLivesOnAvatars(players);

    room.startGame();
    announcementService.sendAnnouncement(`✔️ Game has started`);
  }

  private setLivesOnAvatars(players: Survivor[]): void {
    players.forEach((player) => {
      roomManagementService.setAvatar(player.playerId, `${player.lives}`);
    });
  }

  private setCorrectStadium(noPlayers: number) {
    const stadiumToSet = this.getCorrectStadium(noPlayers);
    roomManagementService.setStadium(stadiumToSet);
  }

  private getCorrectStadium(noPlayers: number): StadiumName {
    switch (noPlayers) {
      case 2:
        return StadiumName.SURVIVAL_2;
      case 3:
        return StadiumName.SURVIVAL_3;
      case 4:
        return StadiumName.SURVIVAL_4;
      case 5:
        return StadiumName.SURVIVAL_5;
      case 6:
        return StadiumName.SURVIVAL_6;
      case 7:
        return StadiumName.SURVIVAL_7;
      case 8:
        return StadiumName.SURVIVAL_8;
      case 9:
        return StadiumName.SURVIVAL_9;
      case 10:
        return StadiumName.SURVIVAL_10;
      default:
        return StadiumName.SURVIVAL_2;
    }
  }

  private needChangeMap(noPlayers: number): boolean {
    const currentStadium = StadiumStore.getCurrentStadium() as Stadium;
    const correctStadiumName = this.getCorrectStadium(noPlayers);

    return correctStadiumName !== currentStadium.name;
  }
}
export const gameManagementService = new GameManagementService();
