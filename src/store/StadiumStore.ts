import { Stadium, StadiumName } from "../models/Stadium";
import { SurvivalStadiumTwo } from "../stadiums/SurvivalStadiumTwo";
import { SurvivalStadiumThree } from "../stadiums/SurvivalStadiumThree";
import { SurvivalStadiumFour } from "../stadiums/SurvivalStadiumFour";
import { SurvivalStadiumFive } from "../stadiums/SurvivalStadiumFive";
import { SurvivalStadiumSix } from "../stadiums/SurvivalStadiumSix";
import { SurvivalStadiumSeven } from "../stadiums/SurvivalStadiumSeven";
import { SurvivalStadiumEight } from "../stadiums/SurvivalStadiumEight";
import { SurvivalStadiumNine } from "../stadiums/SurvivalStadiumNine";
import { SurvivalStadiumTen } from "../stadiums/SurvivalStadiumTen";

export class StadiumStore {
  private static readonly stadiumList: Stadium[] =
    StadiumStore.populateStadiumList();

  public static getCurrentStadium(): Stadium | undefined {
    return this.stadiumList.find((stadium) => stadium.currentStadium);
  }

  public static getStadium(stadiumName: StadiumName): Stadium | undefined {
    return this.stadiumList.find((stadium) => stadium.name === stadiumName);
  }

  public static setCurrentStadium(stadiumName: StadiumName): void {
    if (Object.values(StadiumName).includes(stadiumName)) {
      for (const stadium of StadiumStore.stadiumList) {
        stadium.currentStadium = stadium.name === stadiumName;
      }
    }
  }

  private static populateStadiumList(): Stadium[] {
    return [
      SurvivalStadiumTwo.build(),
      SurvivalStadiumThree.build(),
      SurvivalStadiumFour.build(),
      SurvivalStadiumFive.build(),
      SurvivalStadiumSix.build(),
      SurvivalStadiumSeven.build(),
      SurvivalStadiumEight.build(),
      SurvivalStadiumNine.build(),
      SurvivalStadiumTen.build(),
    ];
  }
}
