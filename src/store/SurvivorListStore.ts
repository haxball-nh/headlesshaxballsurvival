import {
  gameManagementService,
  GameState,
} from "../services/GameManagementService";
import { room } from "../index";
import { announcementService } from "../services/AnnouncementService";
import { Player } from "../models/haxball/Player";
import { roomManagementService } from "../services/RoomManagementService";
import { TimeUtilities } from "../utilities/TimeUtilities";

export class SurvivorListStore {
  private static playerList: Survivor[] = [];

  public static addPlayer(player: Player): void {
    if (
      gameManagementService.currentGameState !== GameState.IN_GAME &&
      this.playerList.filter((survivor) => survivor.playerId === player.id)
        .length === 0 &&
      this.playerList.length < 10
    ) {
      this.playerList.push({ playerId: player.id, lives: 2 });
      room.setPlayerTeam(player.id, 1);

      announcementService.sendAnnouncement(
        `✔️ ${player.name} has entered the lobby (${this.playerList.length}/10)`
      );
    }
  }
  public static getPlayerList(): Survivor[] {
    return this.playerList;
  }
  public static resetPlayerList(): void {
    this.playerList = [];
  }
  public static removePlayer(playerId: number): void {
    this.playerList = this.playerList.filter(
      (survivor) => survivor.playerId !== playerId
    );
  }
  public static async removeLife(goalId: number): Promise<void> {
    const player = this.playerList[goalId - 1];

    // player dies, remove from list
    if (player.lives === 1) {
      this.removePlayer(player.playerId);
      announcementService.sendPlayerEliminatedAnnouncement(
        room.getPlayer(player.playerId).name
      );

      // move to spectators
      room.setPlayerTeam(player.playerId, 0);
    } else {
      // remove life
      this.playerList.filter(
        (survivor) => survivor.playerId === player.playerId
      )[0].lives--;

      announcementService.sendPlayerHurtAnnouncement(
        room.getPlayer(player.playerId).name
      );
      await this.tempSetAvatar(player);
    }

    await this.checkGameOver();
  }
  private static async checkGameOver(): Promise<void> {
    if (this.playerList.length === 1) await gameManagementService.setGameWon();
  }
  private static async tempSetAvatar(player: Survivor): Promise<void> {
    roomManagementService.setAvatar(player.playerId, `🩸`);
    await TimeUtilities.delay(500);
    roomManagementService.setAvatar(player.playerId, `${player.lives}`);
    await TimeUtilities.delay(200);
    roomManagementService.setAvatar(player.playerId, `🩸`);
    await TimeUtilities.delay(500);
    roomManagementService.setAvatar(player.playerId, `${player.lives}`);
    await TimeUtilities.delay(200);
    roomManagementService.setAvatar(player.playerId, `🩸`);
    await TimeUtilities.delay(500);
    roomManagementService.setAvatar(player.playerId, `${player.lives}`);
  }
}

export interface Survivor {
  playerId: number;
  lives: number;
}
