import { registerPlayerEventHandlers } from "./eventHandlers/PlayerEventHandlers";
import { RoomConfig } from "./models/haxball/RoomConfig";
import { Room } from "./models/haxball/Room";
import { registerGameMechanicsEventHandlers } from "./eventHandlers/GameMechanicsEventHandlers";
import { roomManagementService } from "./services/RoomManagementService";

const roomConfig: RoomConfig = {
  geo: { code: "GB", lat: 54.607868, lon: -5.926437 },
  maxPlayers: 25,
  noPlayer: true,
  password: "password",
  public: true,
  roomName: "Room Name",
} as RoomConfig;

// HBInit will exist on page
// @ts-ignore
export const room: Room = HBInit(roomConfig);

roomManagementService.resetRoom();

registerPlayerEventHandlers();
registerGameMechanicsEventHandlers();
